FROM maven:3-openjdk-11 AS build

ENV VERSION=0.9.0

RUN mvn dependency:get -Dartifact=io.strimzi:kafka-oauth-client:$VERSION

FROM confluentinc/cp-schema-registry:7.0.1

ENV TARGET=kafka-oauth

COPY --from=build /root/.m2/repository/com/nimbusds/nimbus-jose-jwt/*/*.jar /usr/share/java/$TARGET/
COPY --from=build /root/.m2/repository/io/strimzi/kafka-oauth-common/*/*.jar /usr/share/java/$TARGET/
COPY --from=build /root/.m2/repository/io/strimzi/kafka-oauth-client/*/*.jar /usr/share/java/$TARGET/

ENV CUB_CLASSPATH="/usr/share/java/$TARGET/*:$CUB_CLASSPATH"
ENV CLASSPATH="/usr/share/java/$TARGET/*"
